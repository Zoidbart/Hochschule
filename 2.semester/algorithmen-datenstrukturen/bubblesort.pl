#!/usr/bin/perl -w
use strict;
use warnings;

my @array = (1,5,2,88,45,29,12,3,3,16);

my @sorted = bubblesort(@array);

sub bubblesort() {
	print("the array to sort is @array\n");
	my $index = 0;
	my $arraysize = scalar @array;
	print("arraysize is $arraysize\n");
	foreach my $element ( @array ) {
		print( "element is $element\n");
		print("index is $index\n");
		if ( $index >= $arraysize) {
				$index = 0;
				print("done one round\n");
				redo;
		} elsif ( $element <= $array[$index+1]) {
			next;
		} else {
			@array[$index,$index+1] = @array[$index,$index+1];
		}
		print("array: @array\n");
		$index ++;
	}
	return( @array);
}
