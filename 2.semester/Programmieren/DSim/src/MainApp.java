import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MainApp extends Application{
	Stage window;
	
	ObservableList<String> discoGuests = FXCollections.observableArrayList();
	ObservableList<String> barGuests = FXCollections.observableArrayList();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("started");
		launch(args);
		System.out.println("stopped");
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		window = primaryStage;
		window.setTitle("disco simulation");
		//Contents of the first HBox
		TextField guestName = new TextField();
		guestName.setPromptText("GastName");
		
		Button addButton = new Button();
		addButton.setText("neuer Gast");
		
		Button clock = new Button();
		clock.setText("Es vergehen 10 minuten");
		
		HBox firstHBox = new HBox(addButton, guestName, clock);
		
		//contents of the second HBox
		TextArea infoPanel = new TextArea();
		ListView<String> guestList = new ListView<>(discoGuests);
		
		ListView<String> bar = new ListView<>(barGuests);
		
		HBox secondHBox = new HBox(infoPanel,guestList, bar);
		
		//contents of the third HBox
		
		VBox progressBox = new VBox();
		
		Pane pain = new Pane();
		pain.setMinSize(300, 300);
		pain.autosize();
		HBox thirdHBox = new HBox(progressBox,pain);
		
		// contents of the fourth HBox
		
		HBox fourthHBox = new HBox();
		
		//initializing the main vbox containing all sub Hboxes
		VBox mainVbox = new VBox(firstHBox, secondHBox, thirdHBox, fourthHBox);
	
		//add everything to everything
		addButton.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				if( !guestName.getText().isEmpty() ) {
					infoPanel.appendText(guestName.getText()+" Betrat die Disco\n");
					discoGuests.add(guestName.getText());
					Text guestT = new Text();
					guestT.setText(guestName.getText());
					guestName.clear();
					guestT.setX(12);
					guestT.setY(13);
					pain.getChildren().add(guestT);
				} else {
					infoPanel.appendText("Niemand Betrat die Disco, \nheuballen rollen durch die Gänge,\nder Türsteher scheint sich geirrt zu haben.\n");
				}
			}
		});
	
		
		// listen for wasd to move persons
		guestList.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				// TODO Auto-generated method stub
				if( event.getCode()==KeyCode.W) {
					System.out.println("walks up\n");
				}else if(event.getCode()==KeyCode.A) {
					System.out.println("walks left\n");
				}else if(event.getCode()==KeyCode.S) {
					System.out.println("walks down\n");
				}else if(event.getCode()==KeyCode.D) {
					System.out.println("walks right\n");
				}else {
					System.out.println("nothing happened\n");
				}
			}
		});

		mainVbox.getChildren().addAll();
		Scene mainScene = new Scene(mainVbox);
		window.setScene(mainScene);
		
		window.show();
	}
}